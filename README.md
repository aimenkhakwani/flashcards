# _JavaScript Flashcards_

### _A fun jQuery game that teaches JavaScript terms and definitions, August 9, 2016_

#### _**By Ewa Manek and Aimen Khakwani**_

## Description

_This simple webpage displays flashcards with JavaScript vocabulary and matching definitions when clicked. jQuery toggle function practice._

##Setup and Installation

* _Clone from GitHub_
* _Run in browser_

## Technologies Used

_HTML, CSS, Bootstrap, jQuery, and JavaScript_

### License
Copyright (c) 2016 **_Aimen Khakwani & Ewa Manek_**
